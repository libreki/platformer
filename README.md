# platformer



## Building

```
git clone https://codeberg.org/libreki/platformer.git
cd platformer
make
```

### Requirements

- [SFML](https://www.sfml-dev.org/)
- [JsonCpp](https://github.com/open-source-parsers/jsoncpp)

## License

[![](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)

GNU General Public License version 3 or any later version.

### Assets

"[PIXEL ART METROIDVANIA ASSET PACK](https://o-lobster.itch.io/platformmetroidvania-pixel-art-asset-pack)"
by [o_lobster](https://o-lobster.itch.io/) is licensed under
[CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)
