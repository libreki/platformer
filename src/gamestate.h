/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "object/character/player.h"
#include "area.h"

class GameState
{
public:
  GameState ();

  Player& get_player ();
  Area& get_area ();

  void add_coin ();
  int get_coins ();

  void add_orb ();
  int get_orbs ();

private:
  Player player;
  Area area;

  int coins = 0;
  int orbs = 0;
};

#endif
