/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "controls.h"
#include "gamestate.h"
#include "graphics/renderer.h"
#include "graphics/textures.h"
#include "object/object.h"
#include "graphics/graphicsconstants.h"

#include <iostream>

int
main ()
{
  sf::RenderWindow window (sf::VideoMode (graphics::SCREEN_WIDTH,
                                          graphics::SCREEN_HEIGHT),
                           "platformer");
  window.setFramerateLimit (60);

  bool focused = true;

  GameState game_state;
  std::unordered_map<textures::ID, sf::Texture> textures
      = textures::load_textures ();

  std::unordered_map<char, sf::Texture> font = textures::load_font ();

  while (window.isOpen ())
    {
      if (focused)
        controls::handle_controls (game_state);

      sf::Event event;
      while (window.pollEvent (event))
        {
          if (event.type == sf::Event::Closed)
            window.close ();

          if (event.type == sf::Event::LostFocus)
            focused = false;

          if (event.type == sf::Event::GainedFocus)
            focused = true;

          if (event.type == sf::Event::KeyPressed)
            {
              if (event.key.code == sf::Keyboard::Space)
                game_state.get_player ().can_double_jump = false;
            }

          if (event.type == sf::Event::KeyReleased)
            {
              if (event.key.code == sf::Keyboard::Space)
                game_state.get_player ().can_double_jump = true;
            }
        }

      game_state.get_area ().update (game_state);
      game_state.get_player ().update (game_state);
      renderer::render_frame (window, textures, font, game_state);
    }

  return 0;
}
