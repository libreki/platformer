/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AREA_H
#define AREA_H

#include <vector>

#include "object/character/enemy/enemy.h"
#include "object/inanimate/collectible.h"
#include "object/inanimate/particle.h"
#include "object/inanimate/tile.h"
#include "object/object.h"

class GameState;

namespace area
{
  struct edges
  {
    double min_x;
    double max_x;
    double min_y;
    double max_y;
  };
}

class Area
{
public:
  Area (const std::string& name);

  void update (GameState& game_state);

  void add_particle (Particle particle);

  void collect_item (std::vector<Collectible>::iterator collectible,
                     GameState& game_state);

  area::edges get_edges ();
  
  std::vector<std::unique_ptr<Enemy> >& get_enemies ();
  std::vector<Particle>& get_particles ();
  std::vector<Collectible>& get_collectibles ();
  const std::vector<Tile>& get_tiles ();

private:
  /* The edges of the area in screen coordinates */
  area::edges edges = { 0, 0, 0, 0 };
  
  std::vector<std::unique_ptr<Enemy>> enemies;
  std::vector<Particle> particles;
  std::vector<Collectible> collectibles;
  std::vector<Tile> tiles;
};

#endif
