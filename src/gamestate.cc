/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "gamestate.h"

#include "logicconstants.h"

GameState::GameState ()
    : player (Pos{ logic::TILE_WIDTH * 3, logic::TILE_WIDTH * 4 }),
      area ("test")
{
}

Player&
GameState::get_player ()
{
  return player;
}

Area&
GameState::get_area ()
{
  return area;
}

void
GameState::add_coin ()
{
  coins++;
}

int
GameState::get_coins ()
{
  return coins;
}

void
GameState::add_orb ()
{
  orbs++;
}

int
GameState::get_orbs ()
{
  return orbs;
}
