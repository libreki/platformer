/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef OBJECT_H
#define OBJECT_H

#include <SFML/Graphics.hpp>

#include "../graphics/textures.h"
#include "../logicconstants.h"

struct Pos
{
  int x;
  int y;
};

struct Dim
{
  int width;
  int height;
};

class Object
{
public:
  Object (Pos pos, Dim dim);

  bool collides_with (Object& object);

  const Pos& get_position () const;
  const Dim& get_dimensions () const;

  virtual textures::ID get_texture_id () = 0;

protected:
  void animation (textures::ID& animation, int& frame_timer,
                  textures::ID first_frame, textures::ID last_frame);

  /* For animations that must always start and end at specific points and
     don't repeat */
  void full_animation (textures::ID& animation, int& frame_timer,
                       textures::ID first_frame, textures::ID last_frame,
                       bool& animation_over);

  Pos position;
  Dim dimensions;

  int frame_timer = logic::FRAME_TIME;
};

#endif
