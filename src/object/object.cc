/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "object.h"

Object::Object (Pos pos, Dim dim) : position (pos), dimensions (dim) {}

bool
Object::collides_with (Object& object)
{
  const Pos& obj_pos = object.get_position ();
  const Dim& obj_dim = object.get_dimensions ();

  bool right_of_obj = position.x >= obj_pos.x + obj_dim.width;
  bool left_of_obj = position.x + dimensions.width <= obj_pos.x;
  bool top_of_obj = position.y + dimensions.height <= obj_pos.y;
  bool bottom_of_obj = position.y >= obj_pos.y + obj_dim.height;

  return !(right_of_obj || left_of_obj || top_of_obj || bottom_of_obj);
}

const Pos&
Object::get_position () const
{
  return position;
}

const Dim&
Object::get_dimensions () const
{
  return dimensions;
}

void
Object::animation (textures::ID& animation, int& frame_timer,
                   textures::ID first_frame, textures::ID last_frame)
{
  if (frame_timer == 0)
    {
      frame_timer = logic::FRAME_TIME;
      if (animation == last_frame)
        animation = first_frame;
      else
        animation = textures::ID (animation + 1);
    }
  else
    {
      frame_timer--;
    }
}

void
Object::full_animation (textures::ID& animation, int& frame_timer,
                        textures::ID first_frame, textures::ID last_frame,
                        bool& animation_over)
{
  if (frame_timer == 0)
    {
      frame_timer = logic::FRAME_TIME;
      if (animation == last_frame)
        {
          animation = first_frame;
          animation_over = !animation_over;
        }
      else
        {
          animation = textures::ID (animation + 1);
        }
    }
  else
    {
      frame_timer--;
    }
}
