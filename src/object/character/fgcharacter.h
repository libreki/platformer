/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef FGCHARACTER_H
#define FGCHARACTER_H

#include "character.h"

namespace fg_character
{
  /* How long should the character's hit animation be played for */
  static const int HIT_TIME = 30;

  /* How long should the character remain invincible after being hit, starts
     counting down at the same time as HIT_TIME */
  static const int INVINCIBILITY_TIME = 60;

  /* How much speed should be added to the vertical component of Velocity per
     tick */
  static const int GRAVITY = 1;

  /* The maximum value of the vertical component of Velocity */
  static const int TERMINAL_VELOCITY = 50;
}

class Area;

class FG_Character : public Character
{
public:
  FG_Character (Pos pos, Dim dim, int hp);

  bool damage (int amount);

  /* Has the character been hit, is the hit animation playing? */
  bool is_hit ();

  bool should_flicker ();

  virtual void handle_movement (GameState& game_state) = 0;

  const int& get_hp () const;

protected:
  void handle_gravity ();

  virtual void set_state () = 0;

  std::string state = "idle";

  /* How long the hit animation should be shown for */
  int hit_timer = 0;

  /* How long the character is invincible after taking damage */
  int invincibility_timer = 0;

  bool invincibility_flicker = false;
  int flicker_timer = logic::FRAME_TIME;

  bool dead = false;

  /* Once the death animation has been played, delete enemy or don't render
     player */
  bool remove = false;

private:
  int hp;
};

#endif
