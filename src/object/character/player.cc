/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "player.h"

#include "../../gamestate.h"
#include "../inanimate/tile.h"
#include "../../logicconstants.h"

Player::Player (Pos pos)
  : FG_Character (pos, Dim{ logic::TILE_WIDTH, logic::TILE_WIDTH }, 5)
{
}

void
Player::update (GameState& game_state)
{
  if (is_hit ())
    hit_timer--;

  if (invincibility_timer > 0)
    invincibility_timer--;

  handle_movement (game_state);

  Area& area = game_state.get_area ();

  for (std::vector<Collectible>::iterator collectible
       = area.get_collectibles ().begin ();
       collectible < area.get_collectibles ().end (); collectible++)
    {
      if (collides_with (*collectible))
        {
          area.collect_item (collectible, game_state);
        }
    }

  for (std::unique_ptr<Enemy>& enemy : area.get_enemies ())
    {
      if (collides_with (*enemy) && !enemy->is_dead ()
          && invincibility_timer <= 0)
        {
          dead = damage (1);
          if (dead)
            {
              velocity.vertical = 0;
              velocity.horizontal = 0;
            }
          else
            {
              hit_timer = fg_character::HIT_TIME;
              invincibility_timer = fg_character::INVINCIBILITY_TIME;
              velocity.vertical = 0;
              velocity.horizontal = 0;
            }
        }
    }
}

void
Player::accelerate (int acceleration)
{
  if (!attacking && !is_hit () && !dead)
    {
      velocity.horizontal += acceleration;

      if (velocity.horizontal > player::MAX_SPEED)
        velocity.horizontal = player::MAX_SPEED;
      else if (velocity.horizontal < -player::MAX_SPEED)
        velocity.horizontal = -player::MAX_SPEED;

      landing = false;
      moved_this_tick = true;
    }
}

void
Player::jump (GameState& game_state)
{
  Area& area = game_state.get_area ();

  if (!airborne && !attacking && !is_hit () && !dead)
    {
      velocity.vertical = player::JUMP_SPEED;
      airborne = true;

      Particle jump_particle (position, dimensions, logic::FRAME_TIME * 4,
                              facing_left, textures::particle_jump_1);

      area.add_particle (jump_particle);
    }
  else if (can_double_jump && airborne && !double_jumping)
    {
      velocity.vertical = player::JUMP_SPEED;
      double_jumping = true;
    }
}

/* Movement is considered independently in vertical and horizontal components
   and collision checks are done independently for the components */
void
Player::handle_movement (GameState& game_state)
{
  Area& area = game_state.get_area ();

  /* Vertical movement */
  position.y += velocity.vertical;

  bool vertical_collision = false;
  for (Tile tile : area.get_tiles ())
    {
      while (collides_with (tile))
        {
          if (velocity.vertical > 0)
            {
              if (airborne)
                landing = true;
              airborne = false; /* Character is on the ground */
              double_jumping = false;
              position.y -= 1;
            }
          else
            {
              position.y += 1;
            }
          vertical_collision = true;
        }
    }

  if (vertical_collision)
    velocity.vertical = 0;
  else
    airborne = true;

  if (landing)
    {
      if (!land_particle_shown)
        {
          Particle land_particle (position, dimensions, logic::FRAME_TIME * 4,
                                  facing_left, textures::particle_land_1);

          area.add_particle (land_particle);
          land_particle_shown = true;
        }
    }
  else
    {
      land_particle_shown = false;
    }

  handle_gravity ();

  /* Horizontal movement */
  position.x += velocity.horizontal;
  if (velocity.horizontal < 0)
    facing_left = true;
  else if (velocity.horizontal > 0)
    facing_left = false;

  pushing = false;
  for (Tile tile : area.get_tiles ())
    {
      while (collides_with (tile))
        {
          if (velocity.horizontal > 0)
            position.x -= 1;
          else
            position.x += 1;
          pushing = true;
        }
    }

  if (!moved_this_tick)
    {
      if (velocity.horizontal > 0)
        {
          velocity.horizontal -= player::DECELERATION;
          if (velocity.horizontal < 0)
            velocity.horizontal = 0;
        }
      else if (velocity.horizontal < 0)
        {
          velocity.horizontal += player::DECELERATION;
          if (velocity.horizontal > 0)
            velocity.horizontal = 0;
        }
    }

  moved_this_tick = false;
}

void
Player::attack (Area& area)
{
  if (!airborne && !attacking)
    {
      attacking = true;
      velocity.horizontal = 0;

      Pos attack_pos = position;
      if (facing_left)
        attack_pos.x -= dimensions.width;
      else
        attack_pos.x += dimensions.width;

      Particle attack_particle (attack_pos, dimensions, logic::FRAME_TIME * 4,
                                facing_left, textures::particle_attack_1,
                                true);

      area.add_particle (attack_particle);
    }
}

textures::ID
Player::get_texture_id ()
{
  set_state ();

  if (state == "run")
    {
      animation (run_state, frame_timer, textures::player_run_1,
                 textures::player_run_6);
      return run_state;
    }

  if (state == "push")
    {
      animation (push_state, frame_timer, textures::player_push_1,
                 textures::player_push_6);
      return push_state;
    }

  if (state == "idle")
    {
      animation (idle_state, frame_timer, textures::player_idle_1,
                 textures::player_idle_4);
      return idle_state;
    }

  if (state == "fall")
    {
      animation (fall_state, frame_timer, textures::player_fall_1,
                 textures::player_fall_3);
      return fall_state;
    }

  if (state == "jump")
    {
      animation (jump_state, frame_timer, textures::player_jump_1,
                 textures::player_jump_3);
      return jump_state;
    }

  if (state == "hit")
    {
      animation (hit_state, frame_timer, textures::player_hit_1,
                 textures::player_hit_3);
      return hit_state;
    }

  if (state == "double_jump")
    {
      animation (double_jump_state, frame_timer,
                 textures::player_double_jump_1,
                 textures::player_double_jump_3);
      return double_jump_state;
    }

  if (state == "land")
    {
      full_animation (land_state, land_frame_timer, textures::player_land_1,
                      textures::player_land_2, landing);
      return land_state;
    }

  if (state == "attack")
    {
      full_animation (attack_state, attack_frame_timer,
                      textures::player_attack_1, textures::player_attack_4,
                      attacking);
      return attack_state;
    }

  if (state == "dead")
    {
      if (remove)
        {
          return textures::none;
        }
      else
        {
          full_animation (dead_state, dead_frame_timer, textures::player_die_1,
                          textures::player_die_8, remove);
          if (remove)
            return textures::none;
          else
            return dead_state;
        }
    }

  /* Should never be called */
  return idle_state;
}

void
Player::set_state ()
{
  std::string previous_state = state;

  if (dead)
    {
      state = "dead";
    }
  else if (is_hit ())
    {
      state = "hit";
    }
  else if (!airborne)
    {
      if (velocity.horizontal != 0)
        {
          if (pushing)
            state = "push";
          else
            state = "run";
        }
      else
        {
          if (attacking)
            state = "attack";
          else if (landing)
            state = "land";
          else
            state = "idle";
        }
    }
  else
    {
      if (double_jumping)
        state = "double_jump";
      else if (velocity.vertical > 0)
        state = "fall";
      else
        state = "jump";
    }

  if (previous_state == "land" && state != "land")
    {
      land_frame_timer = logic::FRAME_TIME;
      land_state = textures::player_land_1;
      landing = false;
    }
}
