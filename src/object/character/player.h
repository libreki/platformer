/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "../inanimate/particle.h"
#include "fgcharacter.h"

namespace player
{
  /* The value the vertical speed is set to when jumping */
  static const int JUMP_SPEED = -20;

  /* The maximum absolute value of the horizontal component of Velocity */
  static const int MAX_SPEED = 10;

  /* How much horizontal speed will be deducted on each tick that the character
     has not moved */
  static const int DECELERATION = 1;
}

class Player : public FG_Character
{
public:
  Player (Pos pos);

  void update (GameState& game_state);

  void accelerate (int acceleration);

  void jump (GameState& game_state);

  void handle_movement (GameState& game_state);

  void attack (Area& area);

  textures::ID get_texture_id ();

  bool can_double_jump = true;
  
protected:
  void set_state ();

private:
  /* Was a movement key pressed / held this tick */
  bool moved_this_tick     = false;

  bool airborne            = false;
  bool double_jumping      = false;
  bool pushing             = false;
  bool landing             = false;
  bool land_particle_shown = false;
  bool attacking           = false;

  
  textures::ID run_state         = textures::player_run_1;
  textures::ID push_state        = textures::player_push_1;
  textures::ID idle_state        = textures::player_idle_1;
  textures::ID fall_state        = textures::player_fall_1;
  textures::ID jump_state        = textures::player_jump_1;
  textures::ID hit_state         = textures::player_hit_1;
  textures::ID double_jump_state = textures::player_double_jump_1;

  /* The below animations always start and end at a specific points so they
     need separate timers */
  
  int land_frame_timer = logic::FRAME_TIME;
  textures::ID land_state = textures::player_land_1;

  int attack_frame_timer = logic::FRAME_TIME;
  textures::ID attack_state = textures::player_attack_1;

  int dead_frame_timer = logic::FRAME_TIME;
  textures::ID dead_state = textures::player_die_1;
};

#endif
