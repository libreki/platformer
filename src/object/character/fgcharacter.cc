/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "fgcharacter.h"

FG_Character::FG_Character (Pos pos, Dim dim, int hp)
    : Character (pos, dim), hp (hp)
{
}

void
FG_Character::handle_gravity ()
{
  if (velocity.vertical < fg_character::TERMINAL_VELOCITY)
    {
      velocity.vertical += fg_character::GRAVITY;
      if (velocity.vertical > fg_character::TERMINAL_VELOCITY)
        velocity.vertical = fg_character::TERMINAL_VELOCITY;
    }
}

bool
FG_Character::damage (int amount)
{
  hp -= amount;
  return hp <= 0;
}

bool
FG_Character::is_hit ()
{
  return (hit_timer > 0);
}

bool
FG_Character::should_flicker ()
{
  if (invincibility_timer > 0 && !is_hit ())
    {
      if (flicker_timer <= 0)
        {
          flicker_timer = logic::FRAME_TIME;
          invincibility_flicker = !invincibility_flicker;
        }
      flicker_timer--;
      return invincibility_flicker;
    }
  return false;
}

const int&
FG_Character::get_hp () const
{
  return hp;
}
