/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SLIME_H
#define SLIME_H

#include "enemy.h"

namespace slime
{
  static const int IDLE_TIME = 180;
  static const int VISION = logic::TILE_WIDTH * 5;
  static const int MOVE_SPEED = 2;
}

class Slime : public Enemy
{
public:
  Slime (Pos pos);

  void update (GameState& game_state);

  double y_render_offset ();
  
  textures::ID get_texture_id ();

protected:
  void set_state ();

private:
  textures::ID idle_state = textures::slime_idle_1;
  textures::ID move_state = textures::slime_move_1;
  textures::ID hit_state  = textures::slime_hit_1;

  /* How long the slime should stay still after moving */
  int idle_timer = slime::IDLE_TIME;

  int dead_frame_timer = logic::FRAME_TIME;
  textures::ID dead_state = textures::slime_die_1;
};

#endif
