/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "enemy.h"

#include "../../../gamestate.h"

Enemy::Enemy (Pos pos, Dim dim, int hp) : FG_Character (pos, dim, hp) {}

bool
Enemy::is_dead ()
{
  return dead;
}

bool
Enemy::should_remove ()
{
  return remove;
}

void
Enemy::accelerate (int to_speed)
{
  if (!is_hit () && !dead)
    velocity.horizontal = to_speed;
}

void
Enemy::handle_movement (GameState& game_state)
{
  Area& area = game_state.get_area ();

  position.x += velocity.horizontal;
  if (velocity.horizontal < 0)
    facing_left = true;
  else if (velocity.horizontal > 0)
    facing_left = false;
  
  for (Tile tile : area.get_tiles ())
    {
      while (collides_with (tile))
        {
          if (velocity.horizontal > 0)
            position.x -= 1;
          else
            position.x += 1;
        }
    }
}

bool
Enemy::is_attacked (Area& area)
{
  for (Particle& particle : area.get_particles ())
    {
      if (particle.is_hurtbox () && collides_with (particle)
          && invincibility_timer <= 0)
        {
          return true;
        }
    }
  return false;
}
