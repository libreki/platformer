/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "slime.h"

#include <random>

#include "../../../gamestate.h"
#include "../../inanimate/tile.h"
#include "../../../logicconstants.h"

Slime::Slime (Pos pos)
  : Enemy (pos, Dim{ logic::TILE_WIDTH, logic::TILE_WIDTH }, 2)
{
}

void
Slime::update (GameState& game_state)
{
  if (hit_timer > 0)
    hit_timer--;
  if (invincibility_timer > 0)
    invincibility_timer--;
  
  if (is_attacked (game_state.get_area ()))
    {
      dead = damage (1);
      hit_timer = fg_character::HIT_TIME;
      invincibility_timer = fg_character::INVINCIBILITY_TIME;
    }
  else if (!is_hit () && !dead)
    {
      if (idle_timer > 0)
        idle_timer--;
      else
        {
          if (!moving)
            {
              int move_speed;
              
              Pos player_pos = game_state.get_player ().get_position ();

              int x_pos_diff = player_pos.x - position.x;
              if (std::abs (x_pos_diff) <= slime::VISION)
                {
                  if (x_pos_diff < 0)
                    move_speed = -slime::MOVE_SPEED;
                  else
                    move_speed = slime::MOVE_SPEED;
                }
              else
                {
                  srand (time (NULL));
                  if (rand () % 2)
                    move_speed = slime::MOVE_SPEED;
                  else
                    move_speed = -slime::MOVE_SPEED;
                }
              
              accelerate (move_speed);
              moving = true;
            }
          handle_movement (game_state);
        }
    }
}

double
Slime::y_render_offset ()
{
  if (state == "move")
    return -dimensions.height;
  else
    return 0;
}

textures::ID
Slime::get_texture_id ()
{
  set_state ();

  if (state == "idle")
    {
      animation (idle_state, frame_timer, textures::slime_idle_1,
                 textures::slime_idle_5);
      return idle_state;
    }

  if (state == "hit")
    {
      animation (hit_state, frame_timer, textures::slime_hit_1,
                 textures::slime_hit_3);
      return hit_state;
    }

  if (state == "move")
    {
      full_animation (move_state, frame_timer, textures::slime_move_1,
                      textures::slime_move_15, moving);
      if (!moving)
        idle_timer = slime::IDLE_TIME;
      
      return move_state;
    }

  if (state == "dead")
    {
      full_animation (dead_state, dead_frame_timer, textures::slime_die_1,
                      textures::slime_die_6, remove);
      if (remove)
        return textures::none;
      else
        return dead_state;
    }

  return idle_state;
}

void
Slime::set_state ()
{
  if (dead)
    state = "dead";
  else if (is_hit ())
    state = "hit";
  else if (moving)
    state = "move";
  else
    state = "idle";
}
