/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "collectible.h"

#include "../../logicconstants.h"

Collectible::Collectible (Pos pos, collectible::type type)
  : Object (Pos{ pos.x + logic::TILE_WIDTH / 4, pos.y + logic::TILE_WIDTH / 4 },
            Dim{ logic::TILE_WIDTH / 2, logic::TILE_WIDTH / 2 }),
      type (type)
{
  if (type == collectible::coin)
    {
      first_frame = textures::coin_1;
      last_frame = textures::coin_6;
      collect = textures::coin_collect_1;
      collect_time = 6;
    }
  else
    {
      first_frame = textures::orb_1;
      last_frame = textures::orb_6;
      collect = textures::orb_collect_1;
      collect_time = 5;
    }
  state = first_frame;
}

collectible::type
Collectible::get_type ()
{
  return type;
}

textures::ID
Collectible::get_texture_id ()
{
  animation (state, frame_timer, first_frame, last_frame);
  return state;
}

textures::ID
Collectible::get_collect ()
{
  return collect;
}

int
Collectible::get_collect_time ()
{
  return collect_time;
}
