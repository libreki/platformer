/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARTICLE_H
#define PARTICLE_H

#include "../object.h"

class Particle : public Object
{
public:
  Particle (Pos pos, Dim dim, int lifetime, bool mirror,
            textures::ID first_frame, bool hurtbox = false);

  /* Returns true once lifetime is over and the particle should be deleted */
  virtual bool advance ();

  bool is_hurtbox ();

  textures::ID get_texture_id ();

  bool mirror_sprite ();

private:
  int lifetime;
  bool mirror;
  bool hurtbox;
  textures::ID state;
};

#endif
