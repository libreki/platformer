/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "particle.h"

Particle::Particle (Pos pos, Dim dim, int lifetime, bool mirror,
                    textures::ID first_frame, bool hurtbox)
  : Object (pos, dim), lifetime (lifetime), mirror (mirror),
    hurtbox (hurtbox), state (first_frame)
{
}

bool
Particle::advance ()
{
  lifetime--;
  if (lifetime == 0)
    return true;
  else
    return false;
}

bool
Particle::is_hurtbox ()
{
  return hurtbox;
}

textures::ID
Particle::get_texture_id ()
{
  if (frame_timer == 0)
    {
      frame_timer = logic::FRAME_TIME;
      state = textures::ID (state + 1);
    }
  else
    {
      frame_timer--;
    }
  return state;
}

bool
Particle::mirror_sprite ()
{
  return mirror;
}
