/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COLLECTIBLE_H
#define COLLECTIBLE_H

#include "../object.h"
#include "tile.h"

namespace collectible
{
  enum type
  {
    coin,
    orb
  };
}

class Collectible : public Object
{
public:
  Collectible (Pos pos, collectible::type type);

  collectible::type get_type ();
  textures::ID get_texture_id ();
  textures::ID get_collect ();
  int get_collect_time ();

private:
  collectible::type type;
  textures::ID state;
  textures::ID first_frame;
  textures::ID last_frame;
  textures::ID collect;
  int collect_time;
};

#endif
