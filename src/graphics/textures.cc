/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "textures.h"

#define COORDS(x, y) std::make_pair (TEXTURE_WIDTH * x, TEXTURE_WIDTH * y)
#define COIN(x, y) std::make_pair (COIN_WIDTH * x, COIN_WIDTH * y)
#define FONT(x, y) std::make_pair (FONT_WIDTH * x, FONT_WIDTH * y)

namespace textures
{
  std::unordered_map<ID, sf::Texture>
  load_textures ()
  {
    std::unordered_map<ID, sf::Texture> textures;

    std::string tileset  = "./data/image/tile_bg_fg/tileset.png";
    std::string player   = "./data/image/herochar/herochar_spritesheet.png";
    std::string coin     = "./data/image/misc/coin_anim_strip_6.png";
    std::string coin_get = "./data/image/misc/coin_pickup_anim_strip_6.png";
    std::string orb      = "./data/image/misc/orb_anim_strip_6.png";
    std::string orb_get  = "./data/image/misc/orb_collected_anim_strip_5.png";
    std::string hud_file = "./data/image/hud/health_menu_hud.png";
    std::string slime    = "./data/image/enemies/slime_spritesheet.png";

    textures[none] = load_texture (tileset, COORDS (10, 5));

    textures[grass_top_left]     = load_texture (tileset, COORDS (0, 0));
    textures[grass_top]          = load_texture (tileset, COORDS (1, 0));
    textures[grass_top_right]    = load_texture (tileset, COORDS (2, 0));
    textures[grass_left]         = load_texture (tileset, COORDS (0, 1));
    textures[grass_center]       = load_texture (tileset, COORDS (1, 1));
    textures[grass_right]        = load_texture (tileset, COORDS (2, 1));
    textures[grass_bottom_left]  = load_texture (tileset, COORDS (0, 2));
    textures[grass_bottom]       = load_texture (tileset, COORDS (1, 2));
    textures[grass_bottom_right] = load_texture (tileset, COORDS (2, 2));

    textures[player_die_1] = load_texture (player, COORDS (0, 0));
    textures[player_die_2] = load_texture (player, COORDS (1, 0));
    textures[player_die_3] = load_texture (player, COORDS (2, 0));
    textures[player_die_4] = load_texture (player, COORDS (3, 0));
    textures[player_die_5] = load_texture (player, COORDS (4, 0));
    textures[player_die_6] = load_texture (player, COORDS (5, 0));
    textures[player_die_7] = load_texture (player, COORDS (6, 0));
    textures[player_die_8] = load_texture (player, COORDS (7, 0));

    textures[player_run_1] = load_texture (player, COORDS (0, 1));
    textures[player_run_2] = load_texture (player, COORDS (1, 1));
    textures[player_run_3] = load_texture (player, COORDS (2, 1));
    textures[player_run_4] = load_texture (player, COORDS (3, 1));
    textures[player_run_5] = load_texture (player, COORDS (4, 1));
    textures[player_run_6] = load_texture (player, COORDS (5, 1));

    textures[player_push_1] = load_texture (player, COORDS (0, 2));
    textures[player_push_2] = load_texture (player, COORDS (1, 2));
    textures[player_push_3] = load_texture (player, COORDS (2, 2));
    textures[player_push_4] = load_texture (player, COORDS (3, 2));
    textures[player_push_5] = load_texture (player, COORDS (4, 2));
    textures[player_push_6] = load_texture (player, COORDS (5, 2));

    textures[player_attack_1] = load_texture (player, COORDS (0, 3));
    textures[player_attack_2] = load_texture (player, COORDS (1, 3));
    textures[player_attack_3] = load_texture (player, COORDS (2, 3));
    textures[player_attack_4] = load_texture (player, COORDS (3, 3));

    textures[player_idle_1] = load_texture (player, COORDS (0, 5));
    textures[player_idle_2] = load_texture (player, COORDS (1, 5));
    textures[player_idle_3] = load_texture (player, COORDS (2, 5));
    textures[player_idle_4] = load_texture (player, COORDS (3, 5));

    textures[player_fall_1] = load_texture (player, COORDS (0, 6));
    textures[player_fall_2] = load_texture (player, COORDS (1, 6));
    textures[player_fall_3] = load_texture (player, COORDS (2, 6));

    textures[player_jump_1] = load_texture (player, COORDS (0, 7));
    textures[player_jump_2] = load_texture (player, COORDS (1, 7));
    textures[player_jump_3] = load_texture (player, COORDS (2, 7));

    textures[player_hit_1] = load_texture (player, COORDS (0, 8));
    textures[player_hit_2] = load_texture (player, COORDS (1, 8));
    textures[player_hit_3] = load_texture (player, COORDS (2, 8));

    textures[player_double_jump_1] = load_texture (player, COORDS (0, 9));
    textures[player_double_jump_2] = load_texture (player, COORDS (1, 9));
    textures[player_double_jump_3] = load_texture (player, COORDS (2, 9));

    textures[player_land_1] = load_texture (player, COORDS (0, 10));
    textures[player_land_2] = load_texture (player, COORDS (1, 10));

    textures[particle_land_1] = load_texture (player, COORDS (0, 12));
    textures[particle_land_2] = load_texture (player, COORDS (1, 12));
    textures[particle_land_3] = load_texture (player, COORDS (2, 12));
    textures[particle_land_4] = load_texture (player, COORDS (3, 12));

    textures[particle_jump_1] = load_texture (player, COORDS (0, 13));
    textures[particle_jump_2] = load_texture (player, COORDS (1, 13));
    textures[particle_jump_3] = load_texture (player, COORDS (2, 13));
    textures[particle_jump_4] = load_texture (player, COORDS (3, 13));

    textures[particle_attack_1] = load_texture (player, COORDS (0, 14));
    textures[particle_attack_2] = load_texture (player, COORDS (1, 14));
    textures[particle_attack_3] = load_texture (player, COORDS (2, 14));
    textures[particle_attack_4] = load_texture (player, COORDS (3, 14));

    int coin_w = COIN_WIDTH;
    textures[coin_1] = load_texture (coin, COIN (0, 0), coin_w, coin_w);
    textures[coin_2] = load_texture (coin, COIN (1, 0), coin_w, coin_w);
    textures[coin_3] = load_texture (coin, COIN (2, 0), coin_w, coin_w);
    textures[coin_4] = load_texture (coin, COIN (3, 0), coin_w, coin_w);
    textures[coin_5] = load_texture (coin, COIN (4, 0), coin_w, coin_w);
    textures[coin_6] = load_texture (coin, COIN (5, 0), coin_w, coin_w);

    textures[coin_collect_1]
        = load_texture (coin_get, COIN (0, 0), coin_w, coin_w * 2);
    textures[coin_collect_2]
        = load_texture (coin_get, COIN (1, 0), coin_w, coin_w * 2);
    textures[coin_collect_3]
        = load_texture (coin_get, COIN (2, 0), coin_w, coin_w * 2);
    textures[coin_collect_4]
        = load_texture (coin_get, COIN (3, 0), coin_w, coin_w * 2);
    textures[coin_collect_5]
        = load_texture (coin_get, COIN (4, 0), coin_w, coin_w * 2);
    textures[coin_collect_6]
        = load_texture (coin_get, COIN (5, 0), coin_w, coin_w * 2);

    textures[orb_1] = load_texture (orb, COIN (0, 0), coin_w, coin_w);
    textures[orb_2] = load_texture (orb, COIN (1, 0), coin_w, coin_w);
    textures[orb_3] = load_texture (orb, COIN (2, 0), coin_w, coin_w);
    textures[orb_4] = load_texture (orb, COIN (3, 0), coin_w, coin_w);
    textures[orb_5] = load_texture (orb, COIN (4, 0), coin_w, coin_w);
    textures[orb_6] = load_texture (orb, COIN (5, 0), coin_w, coin_w);

    textures[orb_collect_1]
        = load_texture (orb_get, COIN (0, 0), coin_w, coin_w);
    textures[orb_collect_2]
        = load_texture (orb_get, COIN (1, 0), coin_w, coin_w);
    textures[orb_collect_3]
        = load_texture (orb_get, COIN (2, 0), coin_w, coin_w);
    textures[orb_collect_4]
        = load_texture (orb_get, COIN (3, 0), coin_w, coin_w);
    textures[orb_collect_5]
        = load_texture (orb_get, COIN (4, 0), coin_w, coin_w);

    textures[hud] = load_texture (hud_file, COORDS (0, 0), 72, 32);

    int slime_w = TEXTURE_WIDTH;
    int slime_h = SLIME_MOVE_HEIGHT;
    textures[slime_move_1]
        = load_texture (slime, COORDS (0, 0), slime_w, slime_h);
    textures[slime_move_2]
        = load_texture (slime, COORDS (1, 0), slime_w, slime_h);
    textures[slime_move_3]
        = load_texture (slime, COORDS (2, 0), slime_w, slime_h);
    textures[slime_move_4]
        = load_texture (slime, COORDS (3, 0), slime_w, slime_h);
    textures[slime_move_5]
        = load_texture (slime, COORDS (4, 0), slime_w, slime_h);
    textures[slime_move_6]
        = load_texture (slime, COORDS (5, 0), slime_w, slime_h);
    textures[slime_move_7]
        = load_texture (slime, COORDS (6, 0), slime_w, slime_h);
    textures[slime_move_8]
        = load_texture (slime, COORDS (7, 0), slime_w, slime_h);
    textures[slime_move_9]
        = load_texture (slime, COORDS (8, 0), slime_w, slime_h);
    textures[slime_move_10]
        = load_texture (slime, COORDS (9, 0), slime_w, slime_h);
    textures[slime_move_11]
        = load_texture (slime, COORDS (10, 0), slime_w, slime_h);
    textures[slime_move_12]
        = load_texture (slime, COORDS (11, 0), slime_w, slime_h);
    textures[slime_move_13]
        = load_texture (slime, COORDS (12, 0), slime_w, slime_h);
    textures[slime_move_14]
        = load_texture (slime, COORDS (13, 0), slime_w, slime_h);
    textures[slime_move_15]
        = load_texture (slime, COORDS (14, 0), slime_w, slime_h);

    textures[slime_die_1] = load_texture (slime, COORDS (0, 2));
    textures[slime_die_2] = load_texture (slime, COORDS (1, 2));
    textures[slime_die_3] = load_texture (slime, COORDS (2, 2));
    textures[slime_die_4] = load_texture (slime, COORDS (3, 2));
    textures[slime_die_5] = load_texture (slime, COORDS (4, 2));
    textures[slime_die_6] = load_texture (slime, COORDS (5, 2));

    textures[slime_idle_1] = load_texture (slime, COORDS (0, 3));
    textures[slime_idle_2] = load_texture (slime, COORDS (1, 3));
    textures[slime_idle_3] = load_texture (slime, COORDS (2, 3));
    textures[slime_idle_4] = load_texture (slime, COORDS (3, 3));
    textures[slime_idle_5] = load_texture (slime, COORDS (4, 3));

    textures[slime_hit_1] = load_texture (slime, COORDS (0, 4));
    textures[slime_hit_2] = load_texture (slime, COORDS (1, 4));
    textures[slime_hit_3] = load_texture (slime, COORDS (2, 4));

    int bg_fg_w = 240;
    int bg_fg_h = 160;
    textures[background_0] = load_texture ("./data/image/tile_bg_fg/bg_0.png",
                                           COORDS (0, 0), bg_fg_w, bg_fg_h);
    textures[background_1] = load_texture ("./data/image/tile_bg_fg/bg_1.png",
                                           COORDS (0, 0), bg_fg_w, bg_fg_h);
    textures[background_2] = load_texture ("./data/image/tile_bg_fg/bg_2.png",
                                           COORDS (0, 0), bg_fg_w, bg_fg_h);
    textures[foreground_0] = load_texture ("./data/image/tile_bg_fg/fg_0.png",
                                           COORDS (0, 0), bg_fg_w, bg_fg_h);
    textures[foreground_1] = load_texture ("./data/image/tile_bg_fg/fg_1.png",
                                           COORDS (0, 0), bg_fg_w, bg_fg_h); 
    
    return textures;
  }

  std::unordered_map<char, sf::Texture>
  load_font ()
  {
    std::unordered_map<char, sf::Texture> font;

    std::string font_file = "./data/image/hud/fonts.png";

    int font_w = FONT_WIDTH;

    font['0'] = load_texture (font_file, FONT (0, 0), font_w, font_w);
    font['1'] = load_texture (font_file, FONT (1, 0), font_w, font_w);
    font['2'] = load_texture (font_file, FONT (2, 0), font_w, font_w);
    font['3'] = load_texture (font_file, FONT (3, 0), font_w, font_w);
    font['4'] = load_texture (font_file, FONT (4, 0), font_w, font_w);
    font['5'] = load_texture (font_file, FONT (5, 0), font_w, font_w);
    font['6'] = load_texture (font_file, FONT (6, 0), font_w, font_w);
    font['7'] = load_texture (font_file, FONT (7, 0), font_w, font_w);
    font['8'] = load_texture (font_file, FONT (8, 0), font_w, font_w);
    font['9'] = load_texture (font_file, FONT (9, 0), font_w, font_w);

    font['a'] = load_texture (font_file, FONT (0, 1), font_w, font_w);
    font['b'] = load_texture (font_file, FONT (1, 1), font_w, font_w);
    font['c'] = load_texture (font_file, FONT (2, 1), font_w, font_w);
    font['d'] = load_texture (font_file, FONT (3, 1), font_w, font_w);
    font['e'] = load_texture (font_file, FONT (4, 1), font_w, font_w);
    font['f'] = load_texture (font_file, FONT (5, 1), font_w, font_w);
    font['g'] = load_texture (font_file, FONT (6, 1), font_w, font_w);
    font['h'] = load_texture (font_file, FONT (7, 1), font_w, font_w);
    font['i'] = load_texture (font_file, FONT (8, 1), font_w, font_w);
    font['j'] = load_texture (font_file, FONT (9, 1), font_w, font_w);

    font['k'] = load_texture (font_file, FONT (0, 2), font_w, font_w);
    font['l'] = load_texture (font_file, FONT (1, 2), font_w, font_w);
    font['m'] = load_texture (font_file, FONT (2, 2), font_w, font_w);
    font['n'] = load_texture (font_file, FONT (3, 2), font_w, font_w);
    font['o'] = load_texture (font_file, FONT (4, 2), font_w, font_w);
    font['p'] = load_texture (font_file, FONT (5, 2), font_w, font_w);
    font['q'] = load_texture (font_file, FONT (6, 2), font_w, font_w);
    font['r'] = load_texture (font_file, FONT (7, 2), font_w, font_w);
    font['s'] = load_texture (font_file, FONT (8, 2), font_w, font_w);
    font['t'] = load_texture (font_file, FONT (9, 2), font_w, font_w);

    font['u'] = load_texture (font_file, FONT (0, 3), font_w, font_w);
    font['v'] = load_texture (font_file, FONT (1, 3), font_w, font_w);
    font['w'] = load_texture (font_file, FONT (2, 3), font_w, font_w);
    font['x'] = load_texture (font_file, FONT (3, 3), font_w, font_w);
    font['y'] = load_texture (font_file, FONT (4, 3), font_w, font_w);
    font['z'] = load_texture (font_file, FONT (5, 3), font_w, font_w);
    font['!'] = load_texture (font_file, FONT (6, 3), font_w, font_w);
    font['?'] = load_texture (font_file, FONT (7, 3), font_w, font_w);
    font['/'] = load_texture (font_file, FONT (8, 3), font_w, font_w);

    return font;
  }

  sf::Texture
  load_texture (const std::string& filename, const std::pair<int, int>& coords,
                int texture_width, int texture_height)
  {
    sf::Texture texture;
    texture.loadFromFile (filename,
                          sf::IntRect (coords.first, coords.second,
                                       texture_width, texture_height));
    texture.setSmooth (false);

    return texture;
  }

  textures::ID
  string_to_texture_id (const std::string& string)
  {
    if (string == "grass_top_left")
      return grass_top_left;
    if (string == "grass_top")
      return grass_top;
    if (string == "grass_top_right")
      return grass_top_right;
    if (string == "grass_left")
      return grass_left;
    if (string == "grass_center")
      return grass_center;
    if (string == "grass_right")
      return grass_right;
    if (string == "grass_bottom_left")
      return grass_bottom_left;
    if (string == "grass_bottom")
      return grass_bottom;
    if (string == "grass_bottom_right")
      return grass_bottom_right;

    return none;
  }
}
