/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GRAPHICS_CONSTANTS_H
#define GRAPHICS_CONSTANTS_H

namespace graphics
{
  static const int SCREEN_WIDTH = 256;
  static const int SCREEN_HEIGHT = 144;

  /* The width of a tile on-screen in pixels using the default 256x144
     resolution */
  static const int TILE_WIDTH = 16;

  /* 240 is the unmodified width of background images in pixels */
  static const float BACKGROUND_SCALE = ((float) SCREEN_WIDTH / (float) 240);
}

#endif
