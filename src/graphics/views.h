/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef VIEWS_H
#define VIEWS_H

#include <SFML/Graphics.hpp>

namespace views
{
  sf::View standard_view;
  sf::View background_0_view;
  sf::View background_1_view;
}

#endif
