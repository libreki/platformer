/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TEXTURES_H
#define TEXTURES_H

#include <unordered_map>

#include <SFML/Graphics.hpp>

namespace textures
{
  /* Texture lengths in pixels */
  static const int TEXTURE_WIDTH = 16;
  static const int COIN_WIDTH = TEXTURE_WIDTH / 2;
  static const int FONT_WIDTH = 7;
  static const int SLIME_MOVE_HEIGHT = TEXTURE_WIDTH * 2;
  
  enum ID
  {
    none,

    grass_top_left, grass_top, grass_top_right,
    grass_left, grass_center, grass_right,
    grass_bottom_left, grass_bottom, grass_bottom_right,

    player_die_1, player_die_2, player_die_3, player_die_4,
    player_die_5, player_die_6, player_die_7, player_die_8,
    player_run_1, player_run_2, player_run_3,
    player_run_4, player_run_5, player_run_6,
    player_push_1, player_push_2, player_push_3,
    player_push_4, player_push_5, player_push_6,
    player_attack_1, player_attack_2, player_attack_3, player_attack_4,
    player_idle_1, player_idle_2, player_idle_3, player_idle_4,
    player_fall_1, player_fall_2, player_fall_3,
    player_jump_1, player_jump_2, player_jump_3,
    player_hit_1, player_hit_2, player_hit_3,
    player_double_jump_1, player_double_jump_2, player_double_jump_3,
    player_land_1, player_land_2,

    particle_land_1, particle_land_2, particle_land_3, particle_land_4,
    particle_jump_1, particle_jump_2, particle_jump_3, particle_jump_4,
    particle_attack_1, particle_attack_2, particle_attack_3, particle_attack_4,

    coin_1, coin_2, coin_3, coin_4, coin_5, coin_6,
    coin_collect_1, coin_collect_2, coin_collect_3,
    coin_collect_4, coin_collect_5, coin_collect_6,
    orb_1, orb_2, orb_3, orb_4, orb_5, orb_6,
    orb_collect_1, orb_collect_2, orb_collect_3, orb_collect_4, orb_collect_5,

    hud,

    slime_move_1, slime_move_2, slime_move_3, slime_move_4, slime_move_5,
    slime_move_6, slime_move_7, slime_move_8, slime_move_9, slime_move_10,
    slime_move_11, slime_move_12, slime_move_13, slime_move_14, slime_move_15,
    slime_die_1, slime_die_2, slime_die_3,
    slime_die_4, slime_die_5, slime_die_6,
    slime_idle_1, slime_idle_2, slime_idle_3, slime_idle_4, slime_idle_5,
    slime_hit_1, slime_hit_2, slime_hit_3,

    background_0, background_1, background_2,
    foreground_0, foreground_1
  };

  std::unordered_map<ID, sf::Texture> load_textures ();

  std::unordered_map<char, sf::Texture> load_font ();

  sf::Texture load_texture (const std::string& filename,
                            const std::pair<int, int>& coords,
                            int texture_width = TEXTURE_WIDTH,
                            int texture_height = TEXTURE_WIDTH);

  textures::ID string_to_texture_id (const std::string& string);
}

#endif
