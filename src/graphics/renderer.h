/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef RENDERER_H
#define RENDERER_H

#include <SFML/Graphics.hpp>

#include "../gamestate.h"
#include "textures.h"

namespace renderer
{
  void render_frame (sf::RenderWindow& window,
                     std::unordered_map<textures::ID, sf::Texture>& textures,
                     std::unordered_map<char, sf::Texture>& font,
                     GameState& game_state);

  void render_background (sf::RenderWindow& window, sf::Texture& texture,
                          double offset_x, int scroll_factor);

  void render_text (sf::RenderWindow& window,
                    std::unordered_map<char, sf::Texture>& font,
                    const std::string& text, float x_0, float y_0);
}

#endif
