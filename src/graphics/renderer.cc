/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "renderer.h"

#include <cmath>

#include "hud.h"
#include "views.h"
#include "graphicsconstants.h"
#include "../logicconstants.h"
#include "../object/character/enemy/slime.h"

namespace renderer
{
  void
  render_frame (sf::RenderWindow& window,
                std::unordered_map<textures::ID, sf::Texture>& textures,
                std::unordered_map<char, sf::Texture>& font,
                GameState& game_state)
  {
    window.clear ();

    Area& area = game_state.get_area ();
    Player& player = game_state.get_player ();
    Pos player_pos = player.get_position ();

    const float tile_width = logic::TILE_WIDTH;
    double offset_x;
    double offset_y;

    offset_x = (player_pos.x / tile_width * graphics::TILE_WIDTH
                - graphics::SCREEN_WIDTH / 2);
    offset_y = 0;

    area::edges edges = area.get_edges ();
    if (offset_x < edges.min_x)
      offset_x = edges.min_x;
    else if ((offset_x + graphics::SCREEN_WIDTH) > edges.max_x)
      offset_x = edges.max_x - graphics::SCREEN_WIDTH;

    /* Draw background */
    views::background_0_view.reset (sf::FloatRect (offset_x / 4,
                                                   offset_y / 4,
                                                   graphics::SCREEN_WIDTH,
                                                   graphics::SCREEN_HEIGHT));
    window.setView (views::background_0_view);
    render_background (window, textures[textures::background_0], offset_x, 4);

    views::background_1_view.reset (sf::FloatRect (offset_x / 2,
                                                   offset_y / 2,
                                                   graphics::SCREEN_WIDTH,
                                                   graphics::SCREEN_HEIGHT));
    window.setView (views::background_1_view);
    render_background (window, textures[textures::background_1], offset_x, 2);
    
    views::standard_view.reset (sf::FloatRect (offset_x, offset_y,
                                               graphics::SCREEN_WIDTH,
                                               graphics::SCREEN_HEIGHT));
    window.setView (views::standard_view);
    
    for (Tile tile : area.get_tiles ())
      {
        sf::Sprite sprite (textures[tile.get_texture_id ()]);
        Pos pos = tile.get_position ();

        double x = pos.x;
        double y = pos.y;

        sprite.setPosition (x / tile_width * graphics::TILE_WIDTH,
                            y / tile_width * graphics::TILE_WIDTH);

        window.draw (sprite);
      }

    for (Collectible& collectible : area.get_collectibles ())
      {
        sf::Sprite sprite (textures[collectible.get_texture_id ()]);
        Pos pos = collectible.get_position ();

        double x = pos.x;
        double y = pos.y;

        sprite.setPosition (x / tile_width * graphics::TILE_WIDTH,
                            y / tile_width * graphics::TILE_WIDTH);

        window.draw (sprite);
      }

    for (std::unique_ptr<Enemy>& enemy : area.get_enemies ())
      {
        if (!enemy->should_flicker ())
          {
            sf::Sprite sprite (textures[enemy->get_texture_id ()]);
            Pos pos = enemy->get_position ();
            double x = pos.x;
            double y = pos.y;
            
            Slime *slime_ptr = (Slime *) enemy.get ();
            if (slime_ptr != nullptr)
              y += slime_ptr->y_render_offset ();
            
            if (enemy->mirror_sprite ())
              {
                sprite.scale (-1.0, 1.0);
                sprite.setPosition ((x + tile_width) / tile_width
                                    * graphics::TILE_WIDTH,
                                    y / tile_width * graphics::TILE_WIDTH);
              }
            else
              {
                sprite.setPosition (x / tile_width * graphics::TILE_WIDTH,
                                    y / tile_width * graphics::TILE_WIDTH);
              }
            
            window.draw (sprite);
          }
      }

    for (Particle& particle : area.get_particles ())
      {
        sf::Sprite sprite (textures[particle.get_texture_id ()]);
        Pos pos = particle.get_position ();

        double x = pos.x;
        double y = pos.y;

        if (particle.mirror_sprite ())
          {
            sprite.scale (-1.0, 1.0);
            sprite.setPosition ((x + tile_width) / tile_width
                                * graphics::TILE_WIDTH,
                                y / tile_width * graphics::TILE_WIDTH);
          }
        else
          {
            sprite.setPosition (x / tile_width * graphics::TILE_WIDTH,
                                y / tile_width * graphics::TILE_WIDTH);
          }
        window.draw (sprite);
      }

    if (!player.should_flicker ())
      {
        sf::Sprite sprite (textures[player.get_texture_id ()]);
        double x = player_pos.x;
        double y = player_pos.y;

        if (player.mirror_sprite ())
          {
            sprite.scale (-1.0, 1.0);
            sprite.setPosition ((x + tile_width) / tile_width
                                * graphics::TILE_WIDTH,
                                y / tile_width * graphics::TILE_WIDTH);
          }
        else
          {
            sprite.setPosition (x / tile_width * graphics::TILE_WIDTH,
                                y / tile_width * graphics::TILE_WIDTH);
          }

        window.draw (sprite);
      }

    window.setView (window.getDefaultView ());
    
    hud::render_hud (window, game_state, textures, font);

    window.display ();
  }

  /* Draws two copies of a background for horizontal scrolling */
  void
  render_background (sf::RenderWindow& window, sf::Texture& texture,
                     double offset_x, int scroll_factor)
  {
    /* "Index" of right background */
    int x_i = std::ceil ( offset_x / graphics::SCREEN_WIDTH / scroll_factor);
    
    int left_background_x = (x_i - 1) * graphics::SCREEN_WIDTH;
    for (int i = 0; i <= 1; i++)
      {
        sf::Sprite background (texture);
        background.setScale (graphics::BACKGROUND_SCALE,
                             graphics::BACKGROUND_SCALE);
        background.setPosition (left_background_x
                                + (i * graphics::SCREEN_WIDTH), 0);
        window.draw (background);
      }
  }
  
  void
  render_text (sf::RenderWindow& window,
               std::unordered_map<char, sf::Texture>& font,
               const std::string& text, float x_0, float y_0)
  {
    float x = x_0;
    float y = y_0;

    for (char c : text)
      {
        if (c != ' ')
          {
            sf::Sprite sprite (font[c]);
            sprite.setPosition (x, y);
            window.draw (sprite);
          }
        x += textures::FONT_WIDTH;
      }
  }
}
