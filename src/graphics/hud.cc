/* -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "hud.h"

#include "renderer.h"

namespace hud
{
  static const int HP_X      = 26;
  static const int HP_Y      = 5;
  static const int HP_WIDTH  = 42;
  static const int HP_HEIGHT = 10;
  
  void
  render_hud (sf::RenderWindow& window, GameState& game_state,
              std::unordered_map<textures::ID, sf::Texture>& textures,
              std::unordered_map<char, sf::Texture>& font)
  {
    sf::Sprite hud (textures[textures::hud]);
    hud.setPosition (0, 0);

    int player_hp = game_state.get_player ().get_hp ();
    if (player_hp < 0)
      player_hp = 0;

    sf::RectangleShape health_bar (
        sf::Vector2f (HP_WIDTH * player_hp / 5.f, HP_HEIGHT));
    health_bar.setPosition (HP_X, HP_Y);
    health_bar.setFillColor (sf::Color (208, 70, 72));

    window.draw (health_bar);
    window.draw (hud);
    renderer::render_text (window, font, std::to_string (player_hp) + "/5",
                           HP_X + 10, HP_Y + 1);

    std::string orbs = std::to_string (game_state.get_orbs ());
    renderer::render_text (window, font, orbs, 35.f, 19.f);
  }
}
