/*  -*- mode: C++; c-file-style: "GNU"; -*-
 *
 *  Copyright (C) 2021-2022  libreki
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "area.h"

#include <fstream>

#include <json/json.h>

#include "gamestate.h"
#include "logicconstants.h"
#include "graphics/graphicsconstants.h"
#include "object/character/enemy/slime.h"

Area::Area (const std::string& name)
{
  std::ifstream level_file ("data/level/" + name + ".json");
  Json::Value level;
  level_file >> level;
  level_file.close ();

  for (Json::Value tile_data : level["tile"])
    {
      int x = tile_data["x_begin"].asInt ();
      int x_end = tile_data["x_end"].asInt ();

      int y_begin = tile_data["y_begin"].asInt ();
      int y_end = tile_data["y_end"].asInt ();

      std::string texture = tile_data["texture"].asString ();

      while (x <= x_end)
        {
          int y = y_begin;
          while (y <= y_end)
            {
              Tile new_tile (Pos{ x * logic::TILE_WIDTH,
                                  y * logic::TILE_WIDTH },
                             textures::string_to_texture_id(texture));
              tiles.push_back (new_tile);

              if (x < edges.min_x)
                edges.min_x = x;
              if (x > edges.max_x)
                edges.max_x = x;
              if (y < edges.min_y)
                edges.min_y = y;
              if (y > edges.max_y)
                edges.max_y = y;
              
              y++;
            }
          x++;
        }
    }

  edges.min_x *= graphics::TILE_WIDTH;
  ++edges.max_x *= graphics::TILE_WIDTH;
  edges.min_y *= graphics::TILE_WIDTH;
  ++edges.max_y *= graphics::TILE_WIDTH;

  Collectible new_coin (Pos{ 5 * logic::TILE_WIDTH, 7 * logic::TILE_WIDTH },
                        collectible::coin);
  collectibles.push_back (new_coin);

  for (int x = 0; x < 16; x++)
    {
      Collectible new_orb (Pos{ x * logic::TILE_WIDTH, 4 * logic::TILE_WIDTH },
                           collectible::orb);
      collectibles.push_back (new_orb);
    }

  enemies.push_back (std::unique_ptr<Enemy> (
      new Slime (Pos{ 10 * logic::TILE_WIDTH, 7 * logic::TILE_WIDTH })));
}

void
Area::update (GameState& game_state)
{
  for (auto particle = particles.begin (); particle < particles.end ();
       particle++)
    {
      bool lifetime_over = particle->advance ();

      if (lifetime_over)
        particles.erase (particle);
    }

  for (unsigned int i = 0; i < enemies.size (); i++)
    {
      enemies[i]->update (game_state);
      if (enemies[i]->should_remove ())
        {
          enemies.erase (enemies.begin () + i);
          i--;
        }
    }
}

void
Area::add_particle (Particle particle)
{
  particles.push_back (particle);
}

void
Area::collect_item (std::vector<Collectible>::iterator collectible,
                     GameState& game_state)
{
  auto c = collectible.base ();

  Pos pos = c->get_position ();
  Dim dim = c->get_dimensions ();

  collectible::type type = c->get_type ();
  if (type == collectible::coin)
    pos.y -= dim.height;

  Particle item_collect (pos, dim, logic::FRAME_TIME * c->get_collect_time (),
                         false, c->get_collect ());
  particles.push_back (item_collect);

  if (type == collectible::coin)
    game_state.add_coin ();
  else
    game_state.add_orb ();

  collectibles.erase (collectible);
}

area::edges
Area::get_edges ()
{
  return edges;
}

std::vector<std::unique_ptr<Enemy> >&
Area::get_enemies ()
{
  return enemies;
}

std::vector<Particle>&
Area::get_particles ()
{
  return particles;
}

std::vector<Collectible>&
Area::get_collectibles ()
{
  return collectibles;
}

const std::vector<Tile>&
Area::get_tiles ()
{
  return tiles;
}
