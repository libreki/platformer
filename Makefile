BIN = platformer

CXXFLAGS = -std=c++17 -g -Wall -Wextra
CPPFLAGS = -c

LDLIBS = -lsfml-graphics -lsfml-window -lsfml-system -ljsoncpp

BUILDDIR = build
OBJDIR = $(BUILDDIR)/obj
SRCDIR = src

SRCS = $(shell find $(SRCDIR) -name '*.cc')
OBJS = $(patsubst %.cc, $(OBJDIR)/%.o, $(SRCS))

.PHONY: all run valgrind clean
all: $(BUILDDIR)/$(BIN)

$(BUILDDIR)/$(BIN): $(OBJS)
	$(CXX) $^ -o $@ $(LDLIBS)

$(OBJDIR)/%.o: %.cc
	@mkdir -p $(dir $@)
	$(CXX) $(CXXFLAGS) $(CPPFLAGS) $< -o $@

run: $(BUILDDIR)/$(BIN)
	$(BUILDDIR)/$(BIN)

valgrind: $(BUILDDIR)/$(BIN)
	valgrind $(BUILDDIR)/$(BIN)

clean:
	rm -rf $(BUILDDIR)
